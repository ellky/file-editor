#!/usr/bin/env sh
set -e
cd /var/www/html

CMD=$1
ARG1=$2
ARG2=$3
ARG3=$4

case "$CMD" in

    "install" )
        composer install
     ;;

    "update" )
        composer update
     ;;

esac

if [ "$ARG1" = "init" ]; then

 	if [ ! -f .env ]; then
 		cp .env.${DOCKER_ENV}.dist .env
 		chmod 0777 .env
 	fi

    until php artisan key:generate; do
        printf 'Generating of application key...'
        sleep 3
    done

    printf 'Application key was successfully generated!'
fi

if [ "$ARG3" = "migrate" ]; then

    /wait-for-it.sh database:3306 -s -t 60 -- echo "Successfully connected to database."

    until php artisan migrate; do
        printf 'Database is seeding by some dummy data...'
        sleep 3
    done

fi

php-fpm

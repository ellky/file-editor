# ------------------------------ PROD ------------------------------------- #
FROM php:7.4-fpm as prod

ARG MODE=dev
ARG RUN_USER_ID=33
ARG RUN_GROUP_ID=33
ARG RUN_USER=www-data
ARG RUN_GROUP=www-data
ARG WWW_PATH=/var/www/html
ENV WEB_ROOT /var/www/html/web

RUN mkdir -p $WEB_ROOT

RUN usermod --non-unique --uid $RUN_USER_ID $RUN_USER
RUN groupmod --non-unique --gid $RUN_GROUP_ID $RUN_GROUP

RUN apt-get update && apt-get install -y --fix-missing \
    apt-utils
#    gnupg

#RUN echo "deb http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
#RUN echo "deb-src http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
#RUN curl -sS --insecure https://www.dotdeb.org/dotdeb.gpg | apt-key add -

# Installing of prod packages.
RUN apt-get update && \apt-get install -y --no-install-recommends \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libxslt-dev \
        libicu-dev \
        libonig-dev \
        libmcrypt-dev \
        libpng-dev \
        libxml2-dev \
        libgmp-dev \
        zlib1g-dev \
        libncurses5-dev \
        libldb-dev \
        libldap2-dev \
        libcurl4-openssl-dev \
        libssl-dev \
        # For RUN pecl install decimal.
#        libmpdec-dev \
#        libssh2-1-dev \
#        librdkafka-dev \
        curl \
        make \
#        git \
#        librabbitmq-dev \
#        libssh-dev \
        libzip-dev \
#        libz-dev \
#        libmemcached-dev \
        && rm -rf /var/lib/apt/lists/*

# Core extensions.
RUN docker-php-ext-install opcache \
    && docker-php-ext-install pdo pdo_mysql \
    && docker-php-ext-install mbstring \
    && docker-php-ext-install bcmath \
#    && docker-php-ext-install sockets \
#    && docker-php-ext-install xsl \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install gd \
#    && docker-php-ext-install xml \
    && docker-php-ext-install curl \
    && docker-php-ext-install zip

# PECL extensions.
#RUN pecl install ssh2-1.0 \
#    && docker-php-ext-enable ssh2

#RUN pecl install decimal

#RUN pecl install mongodb \
#    && pecl install -f rdkafka \
#    && pecl install amqp \
#    && pecl install memcached \
#    && docker-php-ext-enable amqp

# RUN apt-get update && apt-get install -y zlib1g-dev libicu-dev g++
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl

# Configuration of composer.
ENV PATH="/composer/vendor/bin:$PATH" \
    COMPOSER_ALLOW_SUPERUSER=1 \
    COMPOSER_HOME=/composer

RUN curl -s -f -L -o /tmp/installer.php https://getcomposer.org/installer \
 && php /tmp/installer.php --no-ansi --install-dir=/usr/bin --filename=composer \
 && rm /tmp/installer.php \
 && composer --ansi --version --no-interaction


# The final stroke of the wand.
RUN chown -R $RUN_USER:$RUN_GROUP "$WEB_ROOT/../"

WORKDIR $WWW_PATH

RUN mkdir /var/log/php-fpm/
RUN touch /var/log/php-fpm/slowlog.log
RUN chown $RUN_USER:$RUN_GROUP /var/log/php-fpm/slowlog.log

# Coping of scripts and configs.
COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod 0755 /docker-entrypoint.sh

COPY wait-for-it.sh /wait-for-it.sh
RUN chmod 0755 /wait-for-it.sh

COPY config/php.${MODE}.ini /usr/local/etc/php/conf.d/php.ini
ADD config/www.default.conf /usr/local/etc/php-fpm.d/www.default.conf

#RUN echo 'extension=decimal.so' > /usr/local/etc/php/conf.d/decimal.ini
#RUN echo 'extension=mongodb.so' > /usr/local/etc/php/conf.d/mongo.ini
#RUN echo 'extension=rdkafka.so' > /usr/local/etc/php/conf.d/rdkafka.ini
#RUN echo 'extension=memcached.so' > /usr/local/etc/php/conf.d/memcached.ini

ENTRYPOINT ["/docker-entrypoint.sh"]

# ------------------------------ DEV ------------------------------------- #
FROM prod as dev

# Installing of dev packages.
RUN apt-get update && \apt-get install -y --no-install-recommends \
        net-tools \
        # wget \
        vim \
        # zip \
        # unzip \
        && rm -rf /var/lib/apt/lists/*

# PECL extensions.
COPY config/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

RUN pecl install xdebug-2.9.0 && \
    docker-php-ext-enable xdebug

RUN touch /tmp/xdebug.log
RUN chmod 766 /tmp/xdebug.log
RUN chown $RUN_USER:$RUN_GROUP /tmp/xdebug.log
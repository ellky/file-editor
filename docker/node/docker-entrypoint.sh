#!/usr/bin/env sh

npm install

cd /var/www/html

# $0 is a script name,
# $1, $2, $3 etc are passed arguments
# $1 is our command
CMD=$1

case "$CMD" in
  "prod" )
    npm run prod
    ;;

  "watch" )
    npm run watch
    ;;
esac

This project provides an API based on PHP 7.4+ and Laravel 7+ framework.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* need to install ``docker``

    **Notice: See how to: https://docs.docker.com/engine/install/**

* need to install ``docker-compose`` of latest version

    **Notice: See how to: https://docs.docker.com/compose/install/**

### Installation

Checkout the project from git repository:

```
git clone git@bitbucket.org:ellky/file-editor.git
```

## Setup .env file

The project contains environment files which was named like ".env.dev.dist" and ".env.prod.dist".
These files define environment parameters for each environment, f.e: "Development", "Production". 
Every developer has the option to modify this setup file. The modified .env file will be ignore by git ( look into .gitignore ).
Project contain two env files in related directories:
 * ./docker/.env - contain only infrastructure parameters
 * ./app/.env - contain application parameters
 
####Step by step:

1. Move to root folder of project and checkout to appropriate branch

    ```bash
    $ git checkout develop
    ```

2. Copy env file

    ```bash
    $ cp ./docker/.env.dev.dist ./docker/.env
    ```

2. Build application

    ```bash
    $ make app-rebuild
    ```

4. Go to browser by link.

    ```
    http://localhost:80
    ```
    
    **Notice: See ```make``` command help for further commands.**